﻿#pragma strict

//weather or not to render in real time
var RealTime:boolean = false;

//How much of our screen resolution we render at
var RenderResolution:float = 1;

private var renderTexture:Texture2D;
private var lights:Light[];

//Collision Mask
private var collisionMask:LayerMask = 1 << 31;

//Create render texture with screen size with resolution
function Awake() {
	renderTexture = new Texture2D(Screen.width*RenderResolution, Screen.height*RenderResolution);
}

//Do one raytrace when we start playing
function Start() {
    GenerateColliders();
	
	if (!RealTime) {
    	RayTrace();
    	//RTRenderer.SaveTextureToFile(renderTexture, "lolies.png");
    }
}

//Real Time Rendering
function Update() {
	if (RealTime) {
    	RayTrace();
    }
}

//Draw the render
function OnGUI() {
	GUI.DrawTexture(Rect(0, 0, Screen.width, Screen.height), renderTexture);
}

//The function that renders the entire scene to a texture
function RayTrace():void {
	//Gather all lights
	//lights = FindSceneObjectsOfType(typeof(Light)) as Light[];
	lights = FindObjectsOfType(typeof(Light)) as Light[];
	
	for (var x:int = 0; x < renderTexture.width; x += 1) {
    	for (var y:int = 0; y < renderTexture.height; y += 1) {
        	
        	//Now that we have an x/y value for each pixel, we need to make that into a 3d ray
            //according to the camera we are attached to
            var ray:Ray = camera.ScreenPointToRay(Vector3(x/RenderResolution, y/RenderResolution, 0));
            
            //Now lets call a function with this ray and apply it's return value to the pixel we are on
            //We will define this function afterwards
            renderTexture.SetPixel(x, y, TraceRay(ray));
        }
    }
    
    renderTexture.Apply();
}

//Trace a Ray for a singple point
function TraceRay(ray:Ray):Color {
	//The color we change throught the function
	var returnColor:Color = Color.black;
	
	var hit:RaycastHit;
	
	if (Physics.Raycast(ray, hit, Mathf.Infinity, collisionMask)) {
		
	    //The material of the object we hit
	    var mat:Material;
	    
	    //Set the used material
	    mat = hit.collider.transform.parent.renderer.material;
	    
	    //if the material has a texture
		if (mat.mainTexture) {
			//return the color of the pixel at the pixel coordinate of the hit
			returnColor += (mat.mainTexture as Texture2D).GetPixelBilinear(hit.textureCoord.x, hit.textureCoord.y);
		}
		else {
			//return the material color
			returnColor += mat.color;
		}
		
		returnColor *= TraceLight(hit.point + hit.normal*0.0001, hit.normal);
	}
	
	//The color of this pixel
	return returnColor;
}

//Trace a single point for all lights
function TraceLight(pos:Vector3, normal:Vector3):Color {
	//Set starting light to that of the render settings
	var returnColor:Color = RenderSettings.ambientLight;
	
    //We loop through all the lights and perform a light addition with each
	for (var light:Light in lights) {
		if (light.enabled) {
        	//Add the light that this light source casts to the color of this point
			returnColor += LightTrace(light, pos, normal);
		}
	}
	return returnColor;
}

//Trace a single point for a single light
function LightTrace(light:Light, pos:Vector3, normal:Vector3):Color {
	var dot:float;
	
	//Trace the directional light
	if (light.type == LightType.Directional) {
		//calculate the dot product
		dot = Vector3.Dot(-light.transform.forward, normal);
	    
	    //only perform lighting calculations, if the dot is more than 0
	    if (dot > 0) {
			if (Physics.Raycast(pos, -light.transform.forward, Mathf.Infinity, collisionMask)) {
				return Color.black;
			}
	        
	        return light.color*light.intensity*dot;
	    }
		return Color.black;
	}
 	else {
    	var direction:Vector3 = (light.transform.position - pos).normalized;
    	dot = Vector3.Dot(normal, direction);
    	var distance:float = Vector3.Distance(pos, light.transform.position);
        if (distance < light.range && dot > 0) {
    		if (light.type == LightType.Point) {
            	//Raycast as we described
                if (Physics.Raycast(pos, direction, distance, collisionMask)) {
                	return Color.black;
                }
                return light.color*light.intensity*dot*(1 - distance/light.range);
            }
            //Lets check weather we are in the spot or not
            else if (light.type == LightType.Spot) {
            	var dot2:float = Vector3.Dot(-light.transform.forward, direction);
            	if (dot2 > (1 - light.spotAngle/180)) {
	                if (Physics.Raycast(pos, direction, distance, collisionMask)) {
	                	return Color.black;
	                }
	                
	                //We multiply by the multiplier we defined above
	                return light.color*light.intensity*dot*(1 - distance/light.range)*((dot2/(1 - light.spotAngle/180)));
            	}
            }
        }
        return Color.black;
    }
}

//Generate colliders for all objects
function GenerateColliders():void {
	//Loop through all mesh filters
	for (var mf:MeshFilter in FindObjectsOfType(typeof MeshFilter) as MeshFilter[]) {
        if (mf.GetComponent(MeshRenderer)) {
        	//Create a new object we will use for rendering
        	//And make it the same as the MeshFilter
        	var tmpGO:GameObject = GameObject("RTRMeshRenderer");
            tmpGO.AddComponent(MeshCollider).sharedMesh = mf.mesh;
            tmpGO.transform.parent = mf.transform;
            tmpGO.transform.localPosition = Vector3.zero;
            tmpGO.transform.localScale = Vector3.one;
            tmpGO.transform.localRotation = Quaternion.identity;
            
            tmpGO.collider.isTrigger = true;
            tmpGO.layer = 31;
        }
    }
}